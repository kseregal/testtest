/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('TestTest.Application', {
  extend: 'Ext.app.Application',

  name: 'TestTest',

  quickTips: false,
  platformConfig: {
    desktop: {
      quickTips: true
    }
  },

  stores: [
    // TODO: add global / shared stores here
  ],

  launch: function () {
    // TODO - Launch the application
    console.log('aaa')
    let a = [1,2,3]
    let b = [4,5, ...a]
    console.log(a,b)
    const t=1
    h = this.zzz();
    console.log('вернули h', h)

  },


  zzz: async function () {
    try {
      h = await this.yyy(14);
      console.log('h=', h);
      return h;
    } catch (err) {
      console.error('Ошибка' , err)
    }
  },

  yyy: function(t) {
    console.log( 'tt', t )
    return new Ext.Promise( ( resolve, reject ) => {
      if ( t > 0 ) {
        // Есть возможно уже зарегистированая рекламация
        Ext.Msg.confirm('Внимание!', 'test',
            choice  => {
                  if ( choice === 'yes' ) {
                    resolve({x: 'true'});
                  } else resolve({x: 'false'});
          }
        );
      } else resolve({x: 'true'});
    });
  },

  onAppUpdate: function () {
    Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
      function (choice) {
        if (choice === 'yes') {
          window.location.reload();
        }
      }
    );
  }
});
